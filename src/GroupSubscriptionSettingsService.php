<?php

namespace Drupal\group_subscription;

use Drupal\Core\Config\ConfigFactory;

/**
 * Retrieve Group Subscription settings.
 */
class GroupSubscriptionSettingsService {

  /**
   * Integer value for disabled subscriptions.
   */
  const GROUP_SUBSCRIPTION_TYPE_DISABLED = 0;
  
  /**
   * Integer value for immediate subscriptions.
   */
  const GROUP_SUBSCRIPTION_TYPE_IMMEDIATE = 1;

  /**
   * Integer value for daily subscriptions.
   */
  const GROUP_SUBSCRIPTION_TYPE_DAILY = 2;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  public function __construct(ConfigFactory $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Returns whether detailed logging is enabled.
   *
   * @return bool
   *   Whether detailed logging is enabled.
   */
  public function isLoggingEnabled() {
    $moduleSettings = $this->configFactory->get('group_subscription.settings');
    $logging = $moduleSettings->get('group_subscription_watchdog');
    if ($logging) {
      return TRUE;
    }

    return FALSE;
  }

}
