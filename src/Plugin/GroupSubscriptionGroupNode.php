<?php

namespace Drupal\group_subscription\Plugin;

use Drupal\gnode\Plugin\Group\Relation\GroupNode;
use Drupal\Core\Form\FormStateInterface;

class GroupSubscriptionGroupNode extends GroupNode {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $content = $this->pluginDefinition->get('entity_bundle');
    $group_type = $this->getGroupType()->get('label');

    $form['group_subscription'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include in group subscriptions'),
      '#description' => $this->t('Include new @content content in email subscriptions for the @group_type group type.', ['@content' => $content, '@group_type' => $group_type]),
      '#default_value' => $this->configuration['group_subscription'] ?? FALSE,
    ];

    return $form;
  }

}
