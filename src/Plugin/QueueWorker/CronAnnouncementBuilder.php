<?php

namespace Drupal\group_subscription\Plugin\QueueWorker;

/**
 * An Announcement Builder that builds announcement subscriptions on CRON run.
 *
 * @QueueWorker(
 *   id = "cron_announcement_builder",
 *   title = @Translation("Cron Announcement Builder"),
 *   cron = {"time" = 180}
 * )
 */
class CronAnnouncementBuilder extends AnnouncementBuilderBase {}
