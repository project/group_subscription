<?php

namespace Drupal\group_subscription\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeInitialization;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\group_subscription\GroupSubscriptionSettingsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base functionality for the Daily Email Queue Workers.
 */
abstract class DailyEmailBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mail manager.
   *
   * @var Drupal\Core\Mail\MailManager
   */
  protected $mail;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The theme initializer.
   *
   * @var \Drupal\Core\Theme\ThemeInitialization
   */
  protected $themeInitializer;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Constructs a new DailyEmailBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Mail\MailManager $mail
   *   The mail manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Theme\ThemeInitialization $theme_initializer
   *   The theme initializer.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    ConfigFactory $config_factory,
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
    MailManager $mail,
    RendererInterface $renderer,
    ThemeInitialization $theme_initializer,
    ThemeManagerInterface $theme_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->mail = $mail;
    $this->renderer = $renderer;
    $this->themeInitializer = $theme_initializer;
    $this->themeManager = $theme_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.mail'),
      $container->get('renderer'),
      $container->get('theme.initialization'),
      $container->get('theme.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $protoemail = $data;

    // Check the master switch and account status for the recipient.
    $query = $this->database->select('group_subscription_global', 'g');
    $query->join('users_field_data', 'f', 'g.uid = f.uid');
    $query->fields('g', ['uid'])
      ->condition('g.uid', $protoemail->uid)
      ->condition('g.status', 1)
      ->condition('f.status', 1);

    $user = $query->execute()->fetchField();
    if ($user) {
      // Build and send this email.
      $this->sendDailyEmail($protoemail);
    }

  }

  /**
   * Convert this protoemail into an actual email message and send it.
   */
  protected function sendDailyEmail($protoemail) {
    $params = [];

    // Load the subscribed user object to serve as the email recipient.
    $uid = $protoemail->uid;
    $user = $this->entityTypeManager->getStorage('user')->load($uid);

    /** @var \Drupal\user\Entity\User $user */
    $email = $user->getEmail();
    $username = $user->getDisplayName();
    $langcode = $user->getPreferredLangcode();

    // Set recipient display name.
    $params['recipient'] = $username;

    // Add email subject line.
    $site_name = $this->configFactory->get('system.site')->get('name');
    $params['subject'] = $this->t('Your Daily Activity Summary for @site_name',
      ['@site_name' => $site_name]);

    // Determine which theme we should use to render these items.
    $theme_config = $this->configFactory->get('system.theme');
    $default_theme = $theme_config->get('default');
    $this->themeManager->setActiveTheme($this->themeInitializer->initTheme($default_theme));

    // Loop through the groups for this user.
    foreach ($protoemail->groups as $gid => $bundles) {
      $group_content = [];

      $group = $this->entityTypeManager->getStorage('group')->load($gid);
      /** @var \Drupal\group\Entity\Group $group */
      if (is_null($group) || !$group->isPublished()) {
        // This group does not exist or has been unpublished - skip.
        continue;
      }

      // Get the group name.
      $group_name = $group->get('label')->value;

      // Alphabetize bundles and shuffle comments to the end if they exist.
      ksort($protoemail->groups[$gid]);
      if (array_key_exists('Comment', $protoemail->groups[$gid])) {
        $comments = $protoemail->groups[$gid]['Comment'];
        unset($protoemail->groups[$gid]['Comment']);
        $protoemail->groups[$gid]['Comment'] = $comments;
      }

      // Loop through the bundles for this group.
      foreach ($protoemail->groups[$gid] as $bundle => $activity) {
        $bundle_content = [];

        // Get the bundle display name.
        $bundle_name = $bundle;
        $bundle_name .= (substr($bundle_name, -1) == 's') ? '' : 's';

        // Loop through the activity for this bundle.
        foreach ($protoemail->groups[$gid][$bundle] as $activity) {
          // Get entity information so we can determine how to load the entity.
          $entityid = $activity['entityid'];
          $entitytype = $activity['entitytype'];

          // Build the email based on the type of entity we're working with.
          if ($entitytype === 'node') {
            $entity = $this->entityTypeManager->getStorage('node')->load($entityid);
            /** @var \Drupal\node\Entity\Node $entity */
            if (is_null($entity) || !$entity->isPublished()) {
              // This node does not exist or has been unpublished - skip.
              continue;
            }

            $entity_title = $entity->getTitle();
            $author = $entity->getOwner();
            $author_name = $author->getDisplayName();

            $bundle_content[] = Markup::create('<p><i>' . $this->t('@entity_title by @author_name:',
              [
                '@entity_title' => $entity_title,
                '@author_name' => $author_name,
              ]) . '</i></p>');

            // Render the content using group_subscription_daily display.
            $content = $this->entityTypeManager
              ->getViewBuilder($entity->getEntityTypeId())
              ->view($entity, 'group_subscription_daily');
            $bundle_content[] = $this->renderer->renderRoot($content);

            $bundle_content[] = $entity
              ->toLink(NULL, 'canonical', ['absolute' => TRUE])
              ->setText($this->t('View @bundle', ['@bundle' => $bundle]))
              ->toString();

          }
          elseif ($entitytype === 'comment') {
            $entity = $this->entityTypeManager->getStorage('comment')->load($entityid);
            /** @var \Drupal\comment\Entity\Comment $entity */
            if (is_null($entity) || !$entity->isPublished()) {
              // This comment does not exist or has been unpublished - skip.
              continue;
            }

            $entity_title = $entity->getSubject();
            $author = $entity->getOwner();
            $author_name = $author->getDisplayName();

            // Get parent entity information.
            $parentid = $activity['parentid'];
            $parenttype = $activity['parenttype'];

            if ($parenttype === 'node') {
              $parent_entity = $this->entityTypeManager->getStorage('node')->load($parentid);
              /** @var \Drupal\node\Entity\Node $parent_entity */
              if (is_null($parent_entity) || !$parent_entity->isPublished()) {
                // Parent node does not exist or has been unpublished - skip.
                continue;
              }

              $parent_title = $parent_entity->getTitle();
              $parent_bundle = $parent_entity->type->entity->label();
              $parent_link = $parent_entity
                ->toLink(NULL, 'canonical', ['absolute' => TRUE])
                ->setText($this->t('View @parent_bundle', ['@parent_bundle' => $parent_bundle]))
                ->toString();
            }
            else {
              // @todo Extend to handle other entity types.
              $parent_title = '';
              $parent_link = '';
            }

            $bundle_content[] = Markup::create('<p><i>' . $this->t('@author_name commented @entity_title on @parent_title:',
              [
                '@author_name' => $author_name,
                '@entity_title' => $entity_title,
                '@parent_title' => $parent_title,
              ]) . '</i></p>');

            // Render the comment using group_subscription_daily display.
            $comment_content = $this->entityTypeManager
              ->getViewBuilder($entity->getEntityTypeId())
              ->view($entity, 'group_subscription_daily');
            $bundle_content[] = $this->renderer->renderRoot($comment_content);

            // Add link to parent entity for this comment.
            $bundle_content[] = $parent_link;

          }
        }

        // If there is any content for this bundle add it to group_content.
        if (count($bundle_content)) {
          $group_content[] = Markup::create('<h3>' . $this->t('@bundle_name',
            ['@bundle_name' => $bundle_name]) . '</h3>');
          foreach ($bundle_content as $item) {
            $group_content[] = $item;
          }
        }
      }

      // If there is any content for this group add it to body_content.
      if (count($group_content)) {
        $params['body_content'][] = Markup::create('<h2>' . $this->t('Activity for Group: @group_name',
          ['@group_name' => $group_name]) . '</h2>');
        foreach ($group_content as $item) {
          $params['body_content'][] = $item;
        }
      }
    }

    // If the body_content contains items send email, otherwise skip it.
    if (isset($params['body_content'])) {
      // See hook_mail() implementation in group_subscription.module.
      $this->mail->mail(
        'group_subscription',
        'daily_summary',
        $email,
        $langcode ?: 'en',
        $params,
        NULL,
        TRUE
      );
    }

  }

}
