<?php

namespace Drupal\group_subscription\Plugin\QueueWorker;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\group_subscription\GroupSubscriptionSettingsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base functionality for the Announcement Builder Queue Workers.
 */
abstract class AnnouncementBuilderBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The database connection.
   * 
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;
  
  /**
   * The entity type manager.
   * 
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Whether logging is enabled for the module.
   *
   * @var bool
   */
  protected $isLoggingEnabled;

  /**
   * Constructs a new SubscriptionBuilderBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   * @param \Drupal\Core\Queue\QueueInterface $queue
   *   The queue object.
   * @param bool $logging_enabled
   *   Whether logging is enabled for this module.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactoryInterface $logger,
    QueueInterface $queue,
    bool $logging_enabled
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->queue = $queue;
    $this->isLoggingEnabled = $logging_enabled;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('queue')->get('cron_announcement_immediate'),
      $container->get('group_subscription.settings')->isLoggingEnabled()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $activity = $data;

    $entity = $this->entityTypeManager->getStorage('node')->load($data->entityid);
    if (is_null($entity) || !$entity->isPublished()) {
      // This entity does not exist.
      if ($this->isLoggingEnabled) {
        $this->logger->get('group_subscription')->notice('Announcement builder cannot process %entitytype. The %entitytype with ID %entityid is either unpublished or cannot be found. Processing for this announcement will be skipped.',
          ['%entitytype' => $data->entitytype, '%entityid' => $data->entityid]);
      }
      return;
    }

    // Add to announcement immediate queue.
    $this->addToImmediate($activity);

  }

  /**
   * Create an immediate email for each user subscribed to announcements.
   */
  protected function addToImmediate($activity) {
    // Get site-wide subscriptions for active users with master switch on.
    $query = $this->database->select('group_subscription_global', 'g');
    $query->join('users_field_data', 'f', 'g.uid = f.uid');
    $query->fields('g', ['uid']);
    $query->condition('g.status', 1)
      ->condition('g.sitewide', 1)
      ->condition('f.status', 1);

    $users = $query->execute()->fetchAll();
    if (count($users)) {
      // Create a new class to hold the email data.
      $email = new \stdClass();
      $create_count = 0;

      // For each announcement subscription.
      foreach ($users as $user) {
        // Create the email object to be processed.
        $email->uid = $user->uid;
        $email->entityid = $activity->entityid;
        $email->entitytype = $activity->entitytype;
        $email->bundle = $activity->bundle;

        // Add the email object to the queue.
        $created = $this->queue->createItem($email);

        if ($created && $this->isLoggingEnabled) {
          $create_count++;
        }
      }

      if ($this->isLoggingEnabled) {
        $this->logger->get('group_subscription')->notice('Created %createcount out of %attemptcount announcement email subscriptions.',
          ['%createcount' => $create_count, '%attemptcount' => count($users)]);
      }
    }
  }
}
