<?php

namespace Drupal\group_subscription\Plugin\QueueWorker;

/**
 * A Daily Email worker that builds/sends subscription emails on CRON run.
 *
 * @QueueWorker(
 *   id = "cron_subscription_daily",
 *   title = @Translation("Cron Daily Email Processor"),
 *   cron = {"time" = 180}
 * )
 */
class CronDailyEmail extends DailyEmailBase {}
