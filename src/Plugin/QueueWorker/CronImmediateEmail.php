<?php

namespace Drupal\group_subscription\Plugin\QueueWorker;

/**
 * An Immediate Email worker that builds/sends subscription emails on CRON run.
 *
 * @QueueWorker(
 *   id = "cron_subscription_immediate",
 *   title = @Translation("Cron Immediate Email Processor"),
 *   cron = {"time" = 180}
 * )
 */
class CronImmediateEmail extends ImmediateEmailBase {}
