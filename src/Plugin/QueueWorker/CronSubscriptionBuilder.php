<?php

namespace Drupal\group_subscription\Plugin\QueueWorker;

/**
 * A Subscription Builder that builds subscriptions on CRON run.
 *
 * @QueueWorker(
 *   id = "cron_subscription_builder",
 *   title = @Translation("Cron Subscription Builder"),
 *   cron = {"time" = 180}
 * )
 */
class CronSubscriptionBuilder extends SubscriptionBuilderBase {}
