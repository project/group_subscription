<?php

namespace Drupal\group_subscription\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeInitialization;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\group_subscription\GroupSubscriptionSettingsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base functionality for the Immediate Email Queue Workers.
 */
abstract class ImmediateEmailBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mail manager.
   *
   * @var Drupal\Core\Mail\MailManager
   */
  protected $mail;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The theme initializer.
   *
   * @var \Drupal\Core\Theme\ThemeInitialization
   */
  protected $themeInitializer;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Constructs a new ImmediateEmailBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Mail\MailManager $mail
   *   The mail manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Theme\ThemeInitialization $theme_initializer
   *   The theme initializer.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    ConfigFactory $config_factory,
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
    MailManager $mail,
    RendererInterface $renderer,
    ThemeInitialization $theme_initializer,
    ThemeManagerInterface $theme_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->mail = $mail;
    $this->renderer = $renderer;
    $this->themeInitializer = $theme_initializer;
    $this->themeManager = $theme_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.mail'),
      $container->get('renderer'),
      $container->get('theme.initialization'),
      $container->get('theme.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $protoemail = $data;

    // Check the master switch and account status for the recipient.
    $query = $this->database->select('group_subscription_global', 'g');
    $query->join('users_field_data', 'f', 'g.uid = f.uid');
    $query->fields('g', ['uid'])
      ->condition('g.uid', $protoemail->uid)
      ->condition('g.status', 1)
      ->condition('f.status', 1);

    $user = $query->execute()->fetchField();
    if ($user) {
      // Build and send this email.
      $this->sendImmediateEmail($protoemail);
    }

  }

  /**
   * Convert this protoemail into an actual email message and send it.
   */
  protected function sendImmediateEmail($protoemail) {
    $params = [];

    // Load the subscribed user object to serve as the email recipient.
    $uid = $protoemail->uid;
    $user = $this->entityTypeManager->getStorage('user')->load($uid);

    /** @var \Drupal\user\Entity\User $user */
    $email = $user->getEmail();
    $username = $user->getDisplayName();
    $langcode = $user->getPreferredLangcode();

    // Set recipient display name.
    $params['recipient'] = $username;

    // Load the subscribed group object.
    $gid = $protoemail->gid;
    $group = $this->entityTypeManager->getStorage('group')->load($gid);
    /** @var \Drupal\group\Entity\Group $group */
    if (is_null($group) || !$group->isPublished()) {
      // This group does not exist or has been unpublished - skip.
      return;
    }

    // Get the group name.
    $group_name = $group->get('label')->value;

    // Get entity information so we can determine how to load the entity.
    $entityid = $protoemail->entityid;
    $entitytype = $protoemail->entitytype;
    $bundle = $protoemail->bundle;

    // Determine which theme we should use to render these items.
    $theme_config = $this->configFactory->get('system.theme');
    $default_theme = $theme_config->get('default');
    $this->themeManager->setActiveTheme($this->themeInitializer->initTheme($default_theme));

    // Build the email based on the type of entity we're working with.
    if ($entitytype === 'node') {
      $entity = $this->entityTypeManager->getStorage('node')->load($entityid);
      /** @var \Drupal\node\Entity\Node $entity */
      if (is_null($entity) || !$entity->isPublished()) {
        // This node does not exist or has been unpublished - skip.
        return;
      }

      $author = $entity->getOwner();

      // Add email subject line.
      $params['subject'] = $this->t('@group_name: New @bundle: @title by @author',
        [
          '@group_name' => $group_name,
          '@title' => $entity->getTitle(),
          '@bundle' => $bundle,
          '@author' => $author->getDisplayName(),
        ]);

      $params['author'] = $author->getDisplayName();
      $params['group_name'] = $group_name;
      $params['bundle'] = $bundle;

      // Render the content using group_subscription_immediate display.
      $content = $this->entityTypeManager
        ->getViewBuilder($entity->getEntityTypeId())
        ->view($entity, 'group_subscription_immediate');
      $params['content'] = $this->renderer->renderRoot($content);

      $params['link'] = $entity
        ->toLink(NULL, 'canonical', ['absolute' => TRUE])
        ->setText($this->t('View @bundle', ['@bundle' => $bundle]))
        ->toString();

      // See hook_mail() implementation in group_subscription.module.
      $this->mail->mail(
        'group_subscription',
        'immediate_content',
        $email,
        $langcode ?: 'en',
        $params,
        NULL,
        TRUE
      );

    }
    elseif ($entitytype === 'comment') {
      $entity = $this->entityTypeManager->getStorage('comment')->load($entityid);
      /** @var \Drupal\comment\Entity\Comment $entity */
      if (is_null($entity) || !$entity->isPublished()) {
        // This comment does not exist or has been unpublished - skip.
        return;
      }

      $author = $entity->getOwner();
      // @todo Expand to support comments on entities other than nodes.
      if ($protoemail->parenttype !== 'node') {
        return;
      }

      // Comments must be attached to a parent entity, load that information.
      $parent_id = $protoemail->parentid;
      $parent_entity = $this->entityTypeManager->getStorage('node')->load($parent_id);
      /** @var \Drupal\node\Entity\Node $parent_entity */
      $parent_author = $parent_entity->getOwner()->getDisplayName();
      $parent_bundle = $parent_entity->type->entity->label();
      $parent_title = $parent_entity->getTitle();

      // Add email subject line.
      $params['subject'] = $this->t('@group_name: New Comment on @title by @comment_author',
        [
          '@group_name' => $group_name,
          '@title' => $parent_entity->getTitle(),
          '@comment_author' => $author->getDisplayName(),
        ]);

      $params['group_name'] = $group_name;
      $params['parent_author'] = $parent_author;
      $params['parent_type'] = $parent_bundle;
      $params['parent_title'] = $parent_title;

      // Render the parent content using group_subscription_immediate display.
      $parent_content = $this->entityTypeManager
        ->getViewBuilder($parent_entity->getEntityTypeId())
        ->view($parent_entity, 'group_subscription_immediate');
      $params['parent_content'] = $this->renderer->renderRoot($parent_content);

      // Render the comment using group_subscription_immediate display.
      $comment_content = $this->entityTypeManager
        ->getViewBuilder($entity->getEntityTypeId())
        ->view($entity, 'group_subscription_immediate');
      $params['comment_content'] = $this->renderer->renderRoot($comment_content);

      $params['comment_subject'] = $entity->getSubject();
      $params['comment_author'] = $author->getDisplayName();

      $params['link'] = $parent_entity
        ->toLink(NULL, 'canonical', ['absolute' => TRUE])
        ->setText($this->t('View @parent_type', ['@parent_type' => $parent_bundle]))
        ->toString();

      // See hook_mail() implementation in group_subscription.module.
      $this->mail->mail(
        'group_subscription',
        'immediate_comment',
        $email,
        $langcode ?: 'en',
        $params,
        NULL,
        TRUE
      );

    }
    else {
      // TODO: expand to support emails for entities other than nodes.
      return;
    }

  }

}
