<?php

namespace Drupal\group_subscription\Plugin\QueueWorker;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\State\StateInterface;
use Drupal\group_subscription\GroupSubscriptionSettingsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base functionality for the Subscription Builder Queue Workers.
 */
abstract class SubscriptionBuilderBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The state API object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Whether logging is enabled for the module.
   *
   * @var bool
   */
  protected $isLoggingEnabled;

  /**
   * Constructs a new SubscriptionBuilderBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   * @param \Drupal\Core\Queue\QueueInterface $queue
   *   The queue object.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state API object.
   * @param bool $logging_enabled
   *   Whether logging is enabled for this module.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactoryInterface $logger,
    QueueInterface $queue,
    StateInterface $state,
    bool $logging_enabled
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->queue = $queue;
    $this->state = $state;
    $this->isLoggingEnabled = $logging_enabled;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('queue')->get('cron_subscription_immediate'),
      $container->get('state'),
      $container->get('group_subscription.settings')->isLoggingEnabled()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $activity = $data;

    // Check daily subscription queue status.
    $queue_start = $this->state->get('group_subscription.daily_queue_start');
    $queue_start = (is_numeric($queue_start)) ? $queue_start : 0;
    $queue_end = $this->state->get('group_subscription.daily_queue_end');
    $queue_end = (is_numeric($queue_end)) ? $queue_end : 0;

    // Process items if we're not in the middle of sending daily subscriptions.
    if ($queue_start <= $queue_end) {
      // Be sure group specified in $data still exists.
      $group = $this->entityTypeManager->getStorage('group')->load($data->gid);
      /** @var \Drupal\group\Entity\Group $group */
      if (is_null($group) || !$group->isPublished()) {
        // This group does not exist or has been unpublished.
        if ($this->isLoggingEnabled) {
          $this->logger->get('group_subscription')->notice('Subscription builder cannot process group. The group ID %gid is either unpublished or cannot be found. Processing for %entitytype with ID %entityid will be skipped.',
            [
              '%gid' => $data->gid,
              '%entitytype' => $data->entitytype,
              '%entityid' => $data->entityid,
            ]);
        }
        return;
      }

      // Be sure entity specified in $data still exists.
      if ($data->entitytype === 'comment') {
        $comment = $this->entityTypeManager->getStorage('comment')->load($data->entityid);
        /** @var \Drupal\comment\Entity\Comment $comment */
        if (is_null($comment) || !$comment->isPublished()) {
          // This comment does not exist or has been unpublished.
          if ($this->isLoggingEnabled) {
            $this->logger->get('group_subscription')->notice('Subscription builder cannot process %entitytype. The %entitytype with ID %entityid is either unpublished or cannot be found. Processing for this item on %parenttype with ID %parentid in group ID %gid will be skipped.',
              [
                '%entitytype' => $data->entitytype,
                '%entityid' => $data->entityid,
                '%parenttype' => $data->parenttype,
                '%parentid' => $data->parentid,
                '%gid' => $data->gid,
              ]);
          }
          return;
        }
      }
      else {
        $entity = $this->entityTypeManager->getStorage('node')->load($data->entityid);
        /** @var \Drupal\node\Entity\Node $entity */
        if (is_null($entity) || !$entity->isPublished()) {
          // This entity does not exist or has been unpublished.
          if ($this->isLoggingEnabled) {
            $this->logger->get('group_subscription')->notice('Subscription builder cannot process %entitytype. The %entitytype with ID %entityid is either unpublished or cannot be found. Processing for this item in group ID %gid will be skipped.',
              [
                '%entitytype' => $data->entitytype,
                '%entityid' => $data->entityid,
                '%gid' => $data->gid,
              ]);
          }
          return;
        }
      }

      // See if a hold exists for this item.
      $held = $this->database->select('group_subscription_held', 'g')
        ->fields('g', ['entityid', 'entitytype'])
        ->condition('gid', $data->gid)
        ->condition('entityid', $data->entityid)
        ->condition('entitytype', $data->entitytype)
        ->execute()->fetchObject();

      // If no hold exists build subscriptions.
      if (!$held) {
        // Add to group subscription immediate queue.
        $this->addToImmediate($activity);

        // Add to group subscription daily queue.
        $this->addToDaily($activity);
      }
    }
    else {
      // There are items to build but we're processing daily subscription items.
      // Release this item and wait for daily subscriptions to finish sending.
      throw new SuspendQueueException('Group subscription builder waiting for daily subscription sender to complete.');
    }
  }

  /**
   * Create an immediate email for each user subscribed to this activity group.
   */
  protected function addToImmediate($activity) {
    // Get immediate subs for this group for active users with master switch on.
    $query = $this->database->select('group_subscription_groups', 'g');
    $query->join('group_subscription_global', 'gl', 'g.uid = gl.uid');
    $query->join('users_field_data', 'ufd', 'g.uid = ufd.uid');
    $query->fields('g', ['uid'])
      ->condition('g.gid', $activity->gid)
      ->condition('g.subscription', GROUP_SUBSCRIPTION_TYPE_IMMEDIATE)
      ->condition('gl.status', 1)
      ->condition('ufd.status', 1);

    $users = $query->execute()->fetchAll();
    if (count($users)) {
      // Create a new class to hold the email data.
      $email = new \stdClass();
      $create_count = 0;

      // For each immediate subscription with a group id matching this activity.
      foreach ($users as $user) {
        // Create the email object to be processed.
        $email->uid = $user->uid;
        $email->gid = $activity->gid;
        $email->entityid = $activity->entityid;
        $email->entitytype = $activity->entitytype;
        $email->parentid = $activity->parentid;
        $email->parenttype = $activity->parenttype;
        $email->bundle = $activity->bundle;

        // Add the email object to the queue.
        $created = $this->queue->createItem($email);

        if ($created && $this->isLoggingEnabled) {
          $create_count++;
        }
      }

      if ($this->isLoggingEnabled) {
        $this->logger->get('group_subscription')->notice('Immediate builder created %createcount out of %attemptcount immediate email subscriptions for the %entitytype with entityid: %entityid in groupid: %gid.',
          [
            '%createcount' => $create_count,
            '%attemptcount' => count($users),
            '%entitytype' => $activity->entitytype,
            '%entityid' => $activity->entityid,
            '%gid' => $activity->gid,
          ]);
      }
    }
  }

  /**
   * Store a daily email config for each user subscribed to this activity group.
   */
  protected function addToDaily($activity) {
    // Get daily subs for this group for active users with master switch on.
    $query = $this->database->select('group_subscription_groups', 'g');
    $query->join('group_subscription_global', 'gl', 'g.uid = gl.uid');
    $query->join('users_field_data', 'ufd', 'g.uid = ufd.uid');
    $query->fields('g', ['uid'])
      ->condition('g.gid', $activity->gid)
      ->condition('g.subscription', GROUP_SUBSCRIPTION_TYPE_DAILY)
      ->condition('gl.status', 1)
      ->condition('ufd.status', 1);

    $users = $query->execute()->fetchCol();
    if (count($users)) {
      $create_count = 0;

      // See if the entityid already exists in the activity table.
      $query = $this->database->select('group_subscription_activity_daily', 'g');
      $query->fields('g', ['uid'])
        ->condition('g.entityid', $activity->entityid)
        ->condition('g.entitytype', $activity->entitytype);
      $existing = $query->execute()->fetchAll();
      $activity_count = count($existing);

      // If the entityid cannot be found in the activity table add records.
      if ($activity_count === 0) {
        // Field list for database insert.
        $fields = [
          'uid',
          'gid',
          'entityid',
          'entitytype',
          'parentid',
          'parenttype',
          'bundle',
          'timestamp',
        ];

        // Begin creating insert query.
        $insert = $this->database
          ->insert('group_subscription_activity_daily', $options = ['return' => Database::RETURN_AFFECTED])
          ->fields($fields);

        // Cycle through all users to create a single insert query.
        foreach ($users as $user) {
          $data = [
            $user,
            $activity->gid,
            $activity->entityid,
            $activity->entitytype,
            $activity->parentid,
            $activity->parenttype,
            $activity->bundle,
            time(),
          ];

          $insert->values(array_combine($fields, $data));
        }

        $create_count = $insert->execute();

        if ($this->isLoggingEnabled) {
          $this->logger->get('group_subscription')->notice('Daily builder created %createcount out of %attemptcount daily activity records for the %entitytype with entityid: %entityid in groupid: %gid.',
            [
              '%createcount' => $create_count,
              '%attemptcount' => count($users),
              '%entitytype' => $activity->entitytype,
              '%entityid' => $activity->entityid,
              '%gid' => $activity->gid,
            ]);
        }
      }
      else {
        if ($this->isLoggingEnabled) {
          $this->logger->get('group_subscription')->notice('Daily builder attempted to create %attemptcount daily activity records for the %entitytype with entityid: %entityid in groupid: %gid but %activitycount rows matching this entityid were already found.',
            [
              '%attemptcount' => count($users),
              '%entitytype' => $activity->entitytype,
              '%entityid' => $activity->entityid,
              '%gid' => $activity->gid,
              '%activitycount' => $activity_count,
            ]);
        }
      }
    }
  }

}
