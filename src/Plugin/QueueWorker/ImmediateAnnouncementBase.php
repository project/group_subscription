<?php

namespace Drupal\group_subscription\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeInitialization;
use Drupal\Core\Theme\ThemeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base functionality for the Announcement Immediate Email Queue Workers.
 */
abstract class ImmediateAnnouncementBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;
  
  /**
   * The database connection.
   * 
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;
  
  /**
   * The entity type manager.
   * 
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  
  /**
   * The mail manager.
   *
   * @var Drupal\Core\Mail\MailManager
   */
  protected $mail;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;
  
  /**
   * The theme initializer.
   *
   * @var \Drupal\Core\Theme\ThemeInitialization
   */
  protected $themeInitializer;
  
  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Constructs a new ImmediateAnnouncementBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Mail\MailManager $mail
   *   The mail manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Theme\ThemeInitialization $theme_initializer
   *   The theme initializer.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    ConfigFactory $config_factory,
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
    MailManager $mail,
    RendererInterface $renderer,
    ThemeInitialization $theme_initializer,
    ThemeManagerInterface $theme_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    
    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->mail = $mail;
    $this->renderer = $renderer;
    $this->themeInitializer = $theme_initializer;
    $this->themeManager = $theme_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.mail'),
      $container->get('renderer'),
      $container->get('theme.initialization'),
      $container->get('theme.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $protoemail = $data;

    // Check subscription settings and account status for the recipient.
    $query = $this->database->select('group_subscription_global', 'g');
    $query->join('users_field_data', 'f', 'g.uid = f.uid');
    $query->fields('g', ['uid'])
      ->condition('g.uid', $protoemail->uid)
      ->condition('g.status', 1)
      ->condition('g.sitewide', 1)
      ->condition('f.status', 1);

    $user = $query->execute()->fetchField();
    if ($user) {
      // Build and send this email.
      $this->sendImmediateEmail($protoemail);
    }

  }

  /**
   * Convert this protoemail into an actual email message and send it.
   */
  protected function sendImmediateEmail($protoemail) {
    $params = [];

    // Load the subscribed user object to serve as the email recipient.
    $uid = $protoemail->uid;
    $user = $this->entityTypeManager->getStorage('user')->load($uid);

    // User values for email settings.
    $email = $user->getEmail();
    $username = $user->getDisplayName();
    $langcode = $user->getPreferredLangcode();

    // Set recipient display name.
    $params['recipient'] = $username;

    // Get entity information so we can determine how to load the entity.
    $entityid = $protoemail->entityid;
    $entitytype = $protoemail->entitytype;
    $bundle = $protoemail->bundle;

    // Determine which theme we should use to render these items.
    $theme_config = $this->configFactory->get('system.theme');
    $default_theme = $theme_config->get('default');
    $this->themeManager->setActiveTheme($this->themeInitializer->initTheme($default_theme));

    // Build the email based on the type of entity we're working with.
    if ($entitytype === 'node') {
      $entity = $this->entityTypeManager->getStorage('node')->load($entityid);
      if (is_null($entity)) {
        // This entity does not exist.
        return;
      }
      $author = $entity->getOwner();

      // Add email subject line.
      $params['subject'] = $this->t('New Announcement: @title by @author',
        [
          '@title' => $entity->getTitle(),
          '@author' => $author->getDisplayName(),
        ]);

      $params['author'] = $author->getDisplayName();

      // Render the content using group_subscription_announcement display.
      $content = $this->entityTypeManager
        ->getViewBuilder($entity->getEntityTypeId())
        ->view($entity, 'group_subscription_announcement');
      $params['content'] = $this->renderer->renderRoot($content);

      $params['link'] = $entity
        ->toLink(NULL, 'canonical', ['absolute' => TRUE])
        ->setText($this->t('View Announcement'))
        ->toString();

      // See hook_mail() implementation in group_subscription.module.
      $this->mail->mail(
        'group_subscription',
        'announcement',
        $email,
        $langcode ?: 'en',
        $params,
        NULL,
        TRUE
      );

    }
    else {
      // TODO: something strange is afoot track error.
      return;
    }

  }

}
