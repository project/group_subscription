<?php

namespace Drupal\group_subscription\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\group_subscription\GroupSubscriptionSettingsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a block form to capture user settings for a specific group.
 */
class GroupSubscriptionBlockForm extends FormBase {

  /**
   * The database connection.
   * 
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;
  
  /**
   * The account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Whether logging is enabled for the module.
   *
   * @var bool
   */
  protected $isLoggingEnabled;

  /**
   * The group entity.
   *
   * @var \Drupal\group\Entity\GroupInterface
   */
  protected $group;

  /**
   * Constructs a new GroupSubscriptionBlockForm object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The account proxy.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param bool $logging_enabled
   *   Whether logging is enabled for this module.
   */
  public function __construct(
    Connection $database,
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactoryInterface $logger,
    CurrentPathStack $current_path,
    bool $logging_enabled
  ) {
    $this->database = $database;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->currentPath = $current_path;
    $this->isLoggingEnabled = $logging_enabled;
    
    $path = $this->currentPath->getPath();
    $path_parts = explode('/', $path);
    if ($path_parts[1] === 'group') {
      $this->group = $this->entityTypeManager->getStorage('group')->load($path_parts[2]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('path.current'),
      $container->get('group_subscription.settings')->isLoggingEnabled()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_subscription_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // We don't want to build anything unless we are viewing a group entity.
    if ($this->group) {
      // See if the current user is a member of the group we are viewing.
      if ($this->group->getMember($this->currentUser)) {
        // Retrieve group subscription configuration for this user / group.
        $result = $this->database->select('group_subscription_groups', 'g');
        $result->fields('g', ['uid', 'gid', 'groupname', 'subscription']);
        $result->condition('g.uid', $this->currentUser->id());
        $result->condition('g.gid', $this->group->id());
        $group_settings = $result->execute()->fetchObject();

        if ($group_settings) {
          $group_settings = (array) $group_settings;
          $subscription = $group_settings['subscription'];
        }
        else {
          $subscription = GROUP_SUBSCRIPTION_TYPE_DISABLED;
        }

        // Store the current user id.
        $form['uid'] = [
          '#type' => 'value',
          '#value' => $this->currentUser->id(),
        ];

        // Create list of subscription options.
        $options = [
          GROUP_SUBSCRIPTION_TYPE_DISABLED => 'Disabled',
          GROUP_SUBSCRIPTION_TYPE_IMMEDIATE => 'Immediate',
          GROUP_SUBSCRIPTION_TYPE_DAILY => 'Daily',
        ];

        $form['group_subscription'] = [
          '#type' => 'select',
          '#title' => $this->t('Subscription Type'),
          '#default_value' => $subscription,
          '#options' => $options,
        ];

        $form['parent']['actions'] = [
          '#type' => 'actions',
        ];

        $form['parent']['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Update'),
        ];

        return $form;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $uid = $values['uid'];
    $gid = $this->group->id();
    $subscription = $values['group_subscription'];
    $groupname = $this->group->get('label')->value;

    // Check to see if this user has global group_subscription settings.
    $global_settings = $this->database->select('group_subscription_global', 'gg')
      ->fields('gg', ['uid'])
      ->condition('uid', $uid)
      ->execute()->fetchObject();

    if (empty($global_settings)) {
      // Configure global settings with defaults.
      $this->database->insert('group_subscription_global')
        ->fields([
          'uid' => $uid,
          'status' => 1,
          'sitewide' => 0,
        ])
        ->execute();

      // If detailed logging is on, log global settings changes for this user.
      if ($this->isLoggingEnabled) {
        $this->logger->get('group_subscription')->notice('Global settings for user %uid updated. Master switch set to ENABLED, site-wide announcements set to DISABLED.',
          ['%uid' => $uid]);
      }
    }

    // See if user has subscription settings saved for this group.
    $group_settings = $this->database->select('group_subscription_groups', 'g')
      ->fields('g', ['uid'])
      ->condition('uid', $uid)
      ->condition('gid', $gid)
      ->execute()->fetchObject();

    if (empty($group_settings)) {
      // Write new database record for this user / group.
      $this->database->insert('group_subscription_groups')
        ->fields([
          'uid' => $uid,
          'gid' => $gid,
          'groupname' => $groupname,
          'subscription' => $subscription,
        ])
        ->execute();
    }
    else {
      // Update the existing database record for this user / group.
      $this->database->update('group_subscription_groups')
        ->fields([
          'subscription' => $subscription,
        ])
        ->condition('uid', $uid)
        ->condition('gid', $gid)
        ->execute();
    }

    // If detailed logging is on, log group specific changes for this user.
    if ($this->isLoggingEnabled) {
      $status = 'DISABLED';
      switch ($subscription) {
        case GROUP_SUBSCRIPTION_TYPE_IMMEDIATE:
          $status = 'IMMEDIATE';
          break;

        case GROUP_SUBSCRIPTION_TYPE_DAILY:
          $status = 'DAILY';
          break;
      };

      $this->logger->get('group_subscription')->notice('Group settings for user %uid updated. Subscription for %groupname (GID: %gid) set to %subscription.',
        [
          '%uid' => $uid, 
          '%groupname' => $groupname, 
          '%gid' => $gid, 
          '%subscription' => $status,
        ]);
    }

    $this->messenger()->addStatus($this->t('Your group subscription settings for @group_name have been updated.', ['@group_name' => $groupname]));

  }

}
