<?php

namespace Drupal\group_subscription\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form to capture admin settings for the module.
 */
class GroupSubscriptionAdminForm extends ConfigFormBase {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface $currentUser
   */
  protected $currentUser;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The core messenger service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxyInterface $current_user, MessengerInterface $messenger) {
    parent::__construct($config_factory);
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_subscription_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'group_subscription.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('group_subscription.settings');

    $form['group_subscription_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Group Subscription Settings'),
    ];

    $form['group_subscription_settings']['group_subscription_send_hour'] = [
      '#type' => 'select',
      '#title' => $this->t('Daily Start Hour'),
      '#description' => $this->t('Specify the hour (24-hour clock) when daily group subscriptions should be sent.'),
      '#default_value' => $config->get('group_subscription_send_hour'),
      '#options' => [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
        10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        20, 21, 22, 23,
      ],
    ];

    $form['group_subscription_settings']['group_subscription_watchdog'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable detailed logging'),
      '#default_value' => $config->get('group_subscription_watchdog'),
      '#description' => $this->t('If enabled additional group subscription activity will be logged to assist with debugging.'),
    ];

    $form['group_subscription_settings']['group_subscription_subscription_count'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track subscriptions over time'),
      '#default_value' => $config->get('group_subscription_subscription_count'),
      '#description' => $this->t('If enabled the number of subscriptions in each group will be recorded daily.'),
    ];

    $form['group_subscription_sitewide'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable site-wide announcements'),
      '#default_value' => $config->get('group_subscription_sitewide'),
    ];

    $form['group_subscription_sitewide_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Site-wide Announcement Settings'),
      '#states' => [
        'visible' => [
          ':input[name="group_subscription_sitewide"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Grab node types so we can specify one for site-wide announements.
    $node_types = NodeType::loadMultiple();
    $type_options = [];
    foreach ($node_types as $node_type) {
      // TODO: Add check to omit types used for group content.
      $type_options[$node_type->id()] = $node_type->label();
    }

    $form['group_subscription_sitewide_config']['group_subscription_sitewide_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Announcement Type'),
      '#description' => $this->t('Choose a content type to use for site-wide announcements (cannot be used for group content).'),
      '#default_value' => $config->get('group_subscription_sitewide_type'),
      '#options' => $type_options,
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('group_subscription.settings')
      ->set('group_subscription_send_hour', $values['group_subscription_send_hour'])
      ->set('group_subscription_watchdog', $values['group_subscription_watchdog'])
      ->set('group_subscription_subscription_count', $values['group_subscription_subscription_count'])
      ->set('group_subscription_sitewide', $values['group_subscription_sitewide'])
      ->set('group_subscription_sitewide_type', $values['group_subscription_sitewide_type'])
      ->save();
    $this->messenger->addMessage($this->t('Group Subscription admin settings saved.'));
  }

}
