<?php

namespace Drupal\group_subscription\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\group\Entity\Group;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\group_subscription\GroupSubscriptionSettingsService;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form to capture user settings for the module.
 */
class UserSettings extends ConfigFormBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The core messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The group membership loader.
   *
   * @var \Drupal\group\GroupMembershipLoaderInterface
   */
  protected $groupMembershipLoader;

  /**
   * Whether logging is enabled for the module.
   *
   * @var bool
   */
  protected $isLoggingEnabled;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The core messenger service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   * @param \Drupal\group\GroupMembershipLoaderInterface $group_membership_loader
   *   The group membership loader.
   * @param bool $logging_enabled
   *   Whether logging is enabled for the module.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    Connection $database,
    LoggerChannelFactoryInterface $logger,
    MessengerInterface $messenger,
    CurrentRouteMatch $current_route_match,
    GroupMembershipLoaderInterface $group_membership_loader,
    bool $logging_enabled
  ) {
    parent::__construct($config_factory);
    $this->database = $database;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->currentRouteMatch = $current_route_match;
    $this->groupMembershipLoader = $group_membership_loader;
    $this->isLoggingEnabled = $logging_enabled;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('database'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('current_route_match'),
      $container->get('group.membership_loader'),
      $container->get('group_subscription.settings')->isLoggingEnabled()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_subscription_user_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'group_subscription.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $userprofile = $this->currentRouteMatch->getParameter('user');
    $account = User::load($userprofile);
    if (!is_object($account)) {
      return;
    }

    $result = $this->database->select('users', 'u');
    $result->leftjoin('users_field_data', 'ufd', 'u.uid = ufd.uid');
    $result->leftjoin('group_subscription_global', 'g', 'u.uid = g.uid');
    $result->fields('u', ['uid']);
    $result->fields('ufd', ['name', 'mail']);
    $result->fields('g', ['status', 'sitewide']);
    $result->condition('u.uid', $userprofile);
    $result->allowRowCount = TRUE;
    $global_settings = $result->execute()->fetchObject();

    $form = [];

    // Emails cannot be sent if an email address has yet to be supplied.
    if (!$global_settings->mail) {
      $url = '/user/' . $userprofile . '/edit';
      $this->messenger->addMessage($this->t('Your e-mail address must be specified on your <a href="@url">my account</a> page.', ['@url' => $url]), 'error');
    }

    // If user existed before the module was enabled, these are not set.
    if (!isset($global_settings->status)) {
      $global_settings->status = 1;
      $global_settings->sitewide = 0;
    }

    $form['status'] = [
      '#type' => 'radios',
      '#title' => $this->t('Master Switch'),
      '#default_value' => $global_settings->status,
      '#options' => [$this->t('Disabled'), $this->t('Enabled')],
      '#description' => $this->t('This option overrides all other group subscription settings below. To temporarily suspend all email notifications set this to disabled. All additional settings will be preserved.'),
    ];

    $form['sitewide'] = [
      '#type' => 'radios',
      '#title' => $this->t('Site-wide Announcements'),
      '#default_value' => $global_settings->sitewide,
      '#options' => [$this->t('Disabled'), $this->t('Enabled')],
      '#description' => $this->t('Receive email notifications about side-wide announcements.'),
    ];

    $form['group_subscription_groups'] = [
      '#type' => 'details',
      '#title' => $this->t('Group Subscriptions'),
      '#open' => TRUE,
      '#description' => $this->t('Modify subscription settings for each group membership.'),
    ];

    // Create list of subscription options.
    $options = [
      GROUP_SUBSCRIPTION_TYPE_DISABLED => 'Disabled',
      GROUP_SUBSCRIPTION_TYPE_IMMEDIATE => 'Immediate',
      GROUP_SUBSCRIPTION_TYPE_DAILY => 'Daily',
    ];

    // Retrieve stored group subscription configuration for this user.
    $result = $this->database->select('group_subscription_groups', 'g');
    $result->leftjoin('groups_field_data', 'gfd', 'g.gid = gfd.id');
    $result->fields('g', ['uid', 'gid', 'subscription']);
    $result->fields('gfd', ['label']);
    $result->condition('g.uid', $userprofile);
    $result->condition('gfd.status', 1);
    $result->allowRowCount = TRUE;
    $group_settings = $result->execute()->fetchAll();

    // Obtain a list of group memberships for this user.
    $memberships = $this->groupMembershipLoader->loadByUser($account);

    // Check memberships against the stored group subscription configuration.
    if (!empty($memberships)) {
      foreach ($memberships as $membership) {
        $group = $membership->getGroup();
        // Skip any memberships in unpublished groups.
        if (!$group->IsPublished()) {
          continue;
        }
        $gid = $group->id();
        $configured = $this->getSubscriptionSettings($group_settings, $gid);
        // Add a disabled subscription for any untracked groups.
        if (!$configured) {
          $group_settings[] = [
            'uid' => $userprofile,
            'gid' => $gid,
            'label' => $group->get('label')->value,
            'subscription' => GROUP_SUBSCRIPTION_TYPE_DISABLED,
          ];
        }
      }
    }

    // Build form element for each group membership / subscription.
    if (!empty($group_settings)) {
      // Sort group subscription settings by group name alphabetically.
      $keys = array_column($group_settings, 'label');
      array_multisort($keys, SORT_ASC, $group_settings);

      // Create form field to record subscription setting for each group.
      foreach ($group_settings as $group) {
        $group = (array) $group;
        $form['group_subscription_groups']['group_' . $group['gid']] = [
          '#type' => 'select',
          '#title' => $this->t('@group_name', ['@group_name' => $group['label']]),
          '#default_value' => $group['subscription'],
          '#options' => $options,
        ];
      }
    }
    else {
      // User does not belong to any groups.
      $form['group_subscription_groups']['none'] = [
        '#markup' => $this->t('This user is not a member of any groups.'),
      ];
    }

    $form['uid'] = [
      '#type' => 'value',
      '#value' => $userprofile,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $uid = $values['uid'];

    // Write group subscription global settings for user to database.
    $this->database->delete('group_subscription_global')
      ->condition('uid', $uid)
      ->execute();

    $this->database->insert('group_subscription_global')
      ->fields([
        'uid' => $uid,
        'status' => $values['status'],
        'sitewide' => $values['sitewide'],
      ])
      ->execute();

    // If detailed logging is on, log global settings changes for this user.
    if ($this->isLoggingEnabled) {
      $status = ($values['status'] == 1) ? 'ENABLED' : 'DISABLED';
      $sitewide = ($values['sitewide'] == 1) ? 'ENABLED' : 'DISABLED';

      $this->logger->get('group_subscription')->notice('Global settings for user %uid updated. Master switch set to %status, site-wide announcements set to %sitewide.',
        ['%uid' => $uid, '%status' => $status, '%sitewide' => $sitewide]);
    }

    // Write group subscription group settings for user to database.
    foreach ($values as $key => $value) {
      if (strpos($key, 'group_') === 0) {
        $gid = str_replace('group_', '', $key);
        $groupname = Group::load($gid)->get('label')->value;

        $subscription = $this->database->select('group_subscription_groups', 'g')
          ->fields('g', ['uid'])
          ->condition('uid', $uid)
          ->condition('gid', $gid)
          ->execute()->fetchObject();

        if (empty($subscription)) {
          // Write new database record for this user / group.
          $this->database->insert('group_subscription_groups')
            ->fields([
              'uid' => $uid,
              'gid' => $gid,
              'groupname' => $groupname,
              'subscription' => $value,
            ])
            ->execute();

        }
        else {
          // Update the existing database record for this user / group.
          $this->database->update('group_subscription_groups')
            ->fields([
              'subscription' => $value,
            ])
            ->condition('uid', $uid)
            ->condition('gid', $gid)
            ->execute();

        }

        // If detailed logging is on, log group specific changes for this user.
        if ($this->isLoggingEnabled) {
          $subscription = 'DISABLED';
          switch ($value) {
            case GROUP_SUBSCRIPTION_TYPE_IMMEDIATE:
              $subscription = 'IMMEDIATE';
              break;

            case GROUP_SUBSCRIPTION_TYPE_DAILY:
              $subscription = 'DAILY';
              break;
          };

          $this->logger->get('group_subscription')->notice('Group settings for user %uid updated. Subscription for %groupname (GID: %gid) set to %subscription.',
            [
              '%uid' => $uid,
              '%groupname' => $groupname,
              '%gid' => $gid,
              '%subscription' => $subscription,
            ]);
        }
      }
    }

    $this->messenger->addMessage($this->t('Group Subscription settings saved.'));
  }

  /**
   * Checks the existing subscriptions array for the provided group id.
   *
   * @param array $settings
   *   The current group subscription settings for a given user.
   * @param string $gid
   *   The group id to search for.
   *
   * @return bool
   *   Whether the group id was found or not.
   */
  protected function getSubscriptionSettings(array $settings, string $gid) {
    foreach ($settings as $setting) {
      $setting = (array) $setting;
      if ($setting['gid'] === $gid) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
