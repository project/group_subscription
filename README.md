- [Group Subscription](#group-subscription)
  - [Dependencies](#dependencies)
  - [Permissions](#permissions)
  - [Basic Module Settings](#basic-module-settings)
    - [Group Subscription Settings](#group-subscription-settings)
    - [Site-Side Annoucement Settings](#site-side-annoucement-settings)
  - [Enabling Group Subscriptions for Group Content](#enabling-group-subscriptions-for-group-content)
    - [Group Subscriptions for Group Comments](#group-subscriptions-for-group-comments)
  - [Configuring How Group Subscription Emails Look](#configuring-how-group-subscription-emails-look)
    - [Display Recommendations](#display-recommendations)
    - [Announcements](#announcements)
    - [Comments](#comments)
    - [Theming](#theming)
  - [Individual User Settings](#individual-user-settings)
    - [Master Switch](#master-switch)
    - [Site-wide Announcements](#site-wide-announcements)
    - [Group Subscriptions](#group-subscriptions)
    - [Group Subscription Block](#group-subscription-block)
  - [Requeuing Content](#requeuing-content)

# Group Subscription

The Group Subscription module sends email notifications to group members when new content or comments have been posted to their groups. It also sends email notifications to all site users, regardless of group membership, when new site-wide news content has been posted to the site.

There are a few steps involved in setting up the group subscription module to support these tasks. Most of the settings will be configured by a site administrator but end users ultimately control what notifications they receive.

## Dependencies

This module processes and sends email notifications during cron runs. Your webserver will need to have a cron routine configured to run at regular intervals. How to set up a cron routine is outside of the scope of this README. For testing purposes you can manually run cron from the /admin/config/system/cron path in Drupal 8.

Currently this module has been built with the assumption that your site is configured to send HTML emails. This is because the emails sent will be using node displays that include HTML. I prefer using the Mail System and Swift Mailer modules but these specific modules shouldn't be required.

## Permissions

This module adds two site-wide permissions: Access Group Subscription and Administer Group Subscription Settings. You'll need to grant Administer Group Subscription Settings permissons to any role that will be modifying module settings. Roles with this permissions can also modify subscription settings for any user on the site. This can be useful in situations where an end user may not understand how the module works and needs assistance.

You'll also need to grant Access Group Subscription permissions to any role you want to have control over their own subscriptions. In my use case I gave this permission to the authenticated user role so any user with a valid login can control their subscriptions.

This module also adds a group level permission: Omit group content from subscriptions. Any user assigned a group level role with this permission enabled will have the option to omit content from group subscription emails. Checkboxes will be displayed to these users when creating or editing content in groups where their role has this permission. If items are omitted from subscriptions a hold will be placed on them. This makes sure the content item itself isn't included in group subscriptions and any comments placed on the content item are also omitted from group subscriptions.

## Basic Module Settings

Assuming you've already enabled the Group Subscription module, navigate to /admin/config/content/group_subscription. This page is divided into two sections. Group Subscription Settings and Site-Wide Announcement Settings.

### Group Subscription Settings

**Daily Start Hour:** Specify the hour (24-hour clock) when daily group subscriptions will be sent.

Users have the ability to select the type of notifications they would like to receive for each group they belong to. Daily Subscriptions are processed and sent once per day during the time specified here.

**Enable detailed logging:** If enabled additional group subscription activity will be logged to assist with debugging.

With this setting enabled, helpful information will be written to the Drupal logs each time a cron run completes. This information includes things like how many subscriptions were created, how many were sent, and whether certain processes went past their allotted time. This is mostly used for debugging purposes when you're setting up the module in your development environment and should typically be disabled in production.  

**Track subscriptions over time:** If enabled the number of subscriptions in each group will be recorded daily.

**This feature is still in development and is partially implemented.** If this box has been checked, during the time specified in the Daily Start Hour field, daily and immediate subscription counts (the number of users who have signed up for each type of subscription) will be totalled and recorded for each group. There is currently no built-in mechanism for retreiving this data but eventualy I'll provide blocks to display these results. This will give site admins a way to measure engagement for each group over time.

### Site-Side Annoucement Settings

If the Enable site-wide announcements checkbox has been checked you'll see an Announcement Type list box. Use this field to select the content type you'll be using for site-wide announcements (email notifications to be sent to all site users, regardless of the group(s) they belong to). **You should choose a content type that is NOT being used as group content**

Once you've selected a content type for site-wide announcements, and a new node of that type has been created, the module will begin generating email notifications for that new announcement during the next cron run. Announcement emails are sent immediately. There is currently no way schedule a specific time for announcements to go out. 

Remember, your end users ultimately determine the notifications they receive. If none of your end users have site-wide announcements enabled then none will be sent.

## Enabling Group Subscriptions for Group Content

1. Navigate to the group types page at /admin/group/types. This page should list the various group types you've created for your site. The operations column in this table contains a drop down with a list of operations you can perform for each group. 
2. Select the **Set available content** options from this list. You should now be on a page titled: Configure available content. This is where we specify the content types to be used as group content for our group. For my test site I have a group type called Public. When I click on set available content I end up at this url: /admin/group/types/manage/public/content (Your URL should replace the word public with the name of your group type).
3. If you haven't set up any group content types yet you'll need to do that first by clicking the **Install** button from the operations column for the content type you want to use. 
4. If you've already set up a content type as group content the operations column should display a button labeled **Configure**. Click this configure button. The URL you're taken to should look like this: /admin/group/content/manage/public-group_node-bulletin (with public replaced by the name of your group type and bulletin replaced by the name of your group content type)
5. You should see a checkbox at the bottom of this page labeled **Include in group subscriptions** If you check this box new content of this type for this group will generate subscription emails.
6. As noted above in the permissions section, users with appropriate permissions in each group have the ability to omit items from group subscription emails.

Note: these steps must be repeated for each content type / group type combination. For example, if you have a bulletin content type set up as group content in two different group types (public groups and private groups for example), you'll need to perform the steps above twice: once for the public group bulletins and once for the private group bulletins.

### Group Subscriptions for Group Comments

If you've taken the steps to enable group subscriptions for a given group type / content type combination and that content type has comments enabled then subscription emails will also be generated for these comments. There is currently no way to separate these but I do have plans to add the ability to differentiate between group content and group content comments in the future. [Issue #3221309](https://www.drupal.org/project/group_subscription/issues/3221309)

## Configuring How Group Subscription Emails Look

Once we've enabled site-wide announcements and/or group subscriptions for at least one group type / content type combination, we may want to adjust how these items will look in the subscription emails sent out. This module adds several new displays and corresponding twig templates for this purpose. 

1. If you navigate to /admin/structure/types you'll see a list of content types for your site. Click the down arrow next to **Manage fields** in the operations column for the content type you've set up as group content with group subscriptions enabled. Select **Manage display** from the drop down. 
2. I'm sticking with my bulletin content type from my example above so the URL I end up on is: /admin/structure/types/manage/bulletin/display. We need to enable two displays for this content type (if they aren't already). Expand the **Custom Display Settings** at the bottom of the page and enable the following displays: Group Subscription Daily and Group Subscription Immediate.
3. Click save to enable both displays. Now you should see these displays listed as tabs next to the default and teaser options at the top of the page. Modify which fields you want to be included in each type of group subscription email and how you want them to be formatted.
4. For additional customization see the "theming" section below.

### Display Recommendations

How you configure these displays is completely up to you but here are a few things to consider. The Group Subscription Daily display is typically used to show content previews (think of this like a teaser display for subscription emails). The reason for this is simple, if a user is a member of 10 groups and each group has 2 new pieces of content for a given day and that user has selected Daily Subscriptions as their subscription option for each group, then the user is going to receive 1 email with 20 pieces of content using the Group Subscription Daily display. We want these items to be short so the user doesn't end up with a massively long email. Consider omitting unnecessary fields and trimming large items like the body field.

The Group Subscription Immediate display, on the other hand, is less restrictive. Users who opt to subscribe to a given group using Immediate Subscriptions will receive an email immediately following the next cron run after the content has been created. In the example above the user would receive 20 individual emails, each one arriving shortly after the new content was created. It is probably safe to let this display use the entire body field and any supplemental fields you want to include.

### Announcements

Announcements are always sent immediately. There isn't currently a way to include announcements in Daily Subscription emails. Configure your announcement display using steps similar to steps 1-3 above:

1. Select **Manage display** for the content type you're using for announcements.
2. Expand the **Custom Display Settings** at the bottom of this page and enable the Group Subscription Announcement display.
3. Modify this display as you see fit, considering the recommendations for the Group Subscription Immediate display above.
4. For additional customization see the "theming" section below.

### Comments

If you navigate to /admin/structure/comment you'll see a list of comment types for your site. Follow the same steps you took for configuring Group Subscription displays for your comments:

1. Select **Manage display** for the comment type you're using for your group content type.
2. Expand the **Custom Display Settings** at the bottom of this page and enable the Group Subscription Daily (comment) and Group Subscription Immediate (comment) displays
3. Modify these displays as you see fit while considering the display recommendations above.

### Theming

The templates directory for this module includes twig templates for each of the 5 displays mentioned above. Feel free to copy these templates into the front end theme you're using for your site and modify them as you see fit. This module currently sends emails using the enabled front end theme. 

If you need to modify how the entire email looks, not just the individual nodes and comments included within, you'll need to utilize whatever twig templates your mail module provides. If you use Swift Mailer, as one example, you can copy the swiftmailer.html.twig template into your front end theme and modify it.

## Individual User Settings

Enabling the Group Subscription module will add a new tab to user profile pages labeled Group Subscriptions. Users assigned roles with the Access Group Subscription permission will be able to click this tab and modify their Group Subscription settings. This settings page is divided into 3 sections: master switch, site-wide announcements, and group subscriptions.

### Master Switch

If a user sets this value to **Disabled** they won't receive any subscriptions regardless of the rest of their settings. This provides a way for users to temporarily disable all notifications without losing their settings for individual groups. If a user sets this value to **Enabled** the rest of their settings will work as defined.

### Site-wide Announcements

If a user sets this to **Disabled** they will receive no site-wide announcements. If set to **Enabled** they will receive site-wide announcements.

### Group Subscriptions

This section will include a select box for every group the member belongs to. For each group the user can choose to have subscriptions disabled, to receive subscriptions immediately after new content is posted in that group, or have new content notifications for that group summarized in a single daily subscription to be sent at the time specified by the admin on the main Group Subscription settings page.

### Group Subscription Block

This module also includes a group subscription block you can place on your group landing pages. This will give your users a way to modify their subscription settings for a given group without going back to their profile page.

## Requeuing Content

Unpublished announcement and group content nodes will not generate group subscription emails. However, publishing an announcement or group content node that was previously unpublished will add the item to the subscription queue. As a side effect, items can be requeued by unpublishing and republishing them. This is not likely to remain the case for long. In a future release I will add a more refined mechanism for requeuing content.